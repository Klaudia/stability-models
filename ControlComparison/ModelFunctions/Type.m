function [Y0,q,q_downstream,Sb,m] = Type(type,trapezoidal)

if trapezoidal==false;
    if type==1
        Y0=3.5;               %Desired downstream water level [m]
        q=1;                %Steady initial inflow [m^3/s]
        q_downstream=1.5;     %Downstream discharge [m^3/s]
        Sb=0.0003;           %Bottom slope [m/m]
        m=0;                %Side slope [m/m]
    elseif type==3
        Y0=2.275;             %Desired downstream water level [m]
        q=1;                %Steady initial inflow [m^3/s]
        q_downstream=1.5;   %Downstream discharge [m^3/s]
        Sb=0.0003;           %Bottom slope [m/m]
        m=0;                %Side slope [m/m]
    elseif type==4
        Y0=2.05;             %Desired downstream water level [m]
        q=1.5;              %Steady initial inflow [m^3/s]
        q_downstream=2;     %Downstream discharge [m^3/s]
        Sb=0.0003;           %Bottom slope [m/m]
        m=0;                %Side slope [m/m]
    else
        Y0=1.2;             %Desired downstream water level [m]
        q=1;                %Steady initial inflow [m^3/s]
        q_downstream=1.5;   %Downstream discharge [m^3/s]
        Sb=0.0003;           %Bottom slope [m/m]
        m=0;                %Side slope [m/m]
    end
else
     if type==1
        Y0=2.25;             %Desired downstream water level [m]
        q=1;                %Steady initial inflow [m^3/s]
        q_downstream=1.5;   %Downstream discharge [m^3/s]
        Sb=0.00015;         %Bottom slope [m/m]
        m=2;                %Side slope [m/m]
    elseif type==3
        Y0=1.575;            %Desired downstream water level [m]
        q=1.5;              %Steady initial inflow [m^3/s]
        q_downstream=2;     %Downstream discharge [m^3/s]
        Sb=0.00015;         %Bottom slope [m/m]
        m=2;                %Side slope [m/m]
    elseif type==4
        Y0=1.0;             %Desired downstream water level [m]
        q=2;                %Steady initial inflow [m^3/s]
        q_downstream=2.5;     %Downstream discharge [m^3/s]
        Sb=0.00015;         %Bottom slope [m/m]
        m=2;                %Side slope [m/m]
    else
        Y0=0.75;               %Desired downstream water level [m]
        q=0.5;              %Steady initial inflow [m^3/s]
        q_downstream=1;     %Downstream discharge [m^3/s]
        Sb=0.00015;         %Bottom slope [m/m]
        m=2;                %Side slope [m/m]
     end
end