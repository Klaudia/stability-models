function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel, Error_max, SettleTime, Error_mean,a, b, c]=Fun_ClosedLoopControl_IR_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,Mr_db,Om_r,UseTravelTime,factor_sim_time,eMAVE,duMAVE);
% clear
% n=0.02;                     %Manning factor [-]
% Bw=3;                       %Bottom width [m]
% 
% close all
% 
% L=7000;
% q=1;
% m=0;
% Sb=0.0001;
% Y0=1.2;
% h_real_initial=Y0;
% q_downstream=1.5;
% factor_sim_time=30;
% eMAVE=5;
% duMAVE=1;
% kh=0;
% UseTravelTime=0;
% Mr_db=0;
% Om_r=0;
 [ae tau T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);

%% tuning variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %simulation time [s]
%simulation_time=3600*20;
%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand

%Internal IR-model
[num,den,sysidr,Mr_calc,Om_r_calc,num2,Om_r_calc_tr,DR]=ident_idr_new(Bw,Y0,m,q,L,n,Mr_db,Om_r,UseTravelTime);
omega=Om_r_calc;
zeta=DR;
A=omega^2/num;    %Backwater surface

k1=1/(zeta*omega+omega*(zeta^2-1)^0.5);
k2=1/(zeta*omega-omega*(zeta^2-1)^0.5);

d2=-1-exp(-sampling_time/k1)-exp(-sampling_time/k2);
d1=exp(-sampling_time/k1)*exp(-sampling_time/k2)+exp(-sampling_time/k1)+exp(-sampling_time/k2);
d0=-exp(-sampling_time/k1)*exp(-sampling_time/k2);

%upstream flow
a_i=0;
b_i=0;
c_i=1/A;

a1_i=-(-a_i*k1+b_i*k1^2-k1^3*c_i)/(k1-k2)/k1;
a2_i=(-a_i*k2+b_i*k2^2-k2^3*c_i)/(k1-k2)/k2;
a3_i=b_i-c_i*(k1+k2);
a4_i=c_i;

n2_i=-2*a1_i-a1_i*exp(-sampling_time/k2)-2*a2_i-a2_i*exp(-sampling_time/k1)-a3_i*(exp(-sampling_time/k1)+exp(-sampling_time/k2))- a3_i+sampling_time*a4_i;
n1_i=a1_i+a2_i+2*a1_i*exp(-sampling_time/k2)+2*a2_i*exp(-sampling_time/k1)+a3_i*exp(-sampling_time/k1)*exp(-sampling_time/k2)+ a3_i*(exp(-sampling_time/k1)+exp(-sampling_time/k2))-sampling_time*a4_i*(exp(-sampling_time/k1)+exp(-sampling_time/k2));
n0_i=-a1_i*exp(-sampling_time/k2)-a2_i*exp(-sampling_time/k1)-a3_i*exp(-sampling_time/k1)*exp(-sampling_time/k2)+sampling_time*a4_i*exp(-sampling_time/k1)*exp(-sampling_time/k2);

%Downstream flow
a_o=-2/A/omega/omega;
b_o=-4*zeta/A/omega;
c_o=-1/A;

a1_o=-(-a_o*k1+b_o*k1^2-k1^3*c_o)/(k1-k2)/k1;
a2_o=(-a_o*k2+b_o*k2^2-k2^3*c_o)/(k1-k2)/k2;
a3_o=b_o-c_o*(k1+k2);
a4_o=c_o;

n2_o=-2*a1_o-a1_o*exp(-sampling_time/k2)-2*a2_o-a2_o*exp(-sampling_time/k1)-a3_o*(exp(-sampling_time/k1)+exp(-sampling_time/k2))- a3_o+sampling_time*a4_o;
n1_o=a1_o+a2_o+2*a1_o*exp(-sampling_time/k2)+2*a2_o*exp(-sampling_time/k1)+a3_o*exp(-sampling_time/k1)*exp(-sampling_time/k2)+ a3_o*(exp(-sampling_time/k1)+exp(-sampling_time/k2))-sampling_time*a4_o*(exp(-sampling_time/k1)+exp(-sampling_time/k2));
n0_o=-a1_o*exp(-sampling_time/k2)-a2_o*exp(-sampling_time/k1)-a3_o*exp(-sampling_time/k1)*exp(-sampling_time/k2)+sampling_time*a4_o*exp(-sampling_time/k1)*exp(-sampling_time/k2);

% constructing matrices
system_size=7;            %system size is alwas 7
a=zeros(system_size,system_size);
a(1,1)=(-d2);
a(1,2)=-d1;
a(1,3)=-d0;
a(1,4)=n1_i;
a(1,5)=n0_i;
a(1,6)=n1_o;
a(1,7)=n0_o;
a(2,1)=1;
a(3,2)=1;
a(5,4)=1;
a(7,6)=1;
%to get only real numbers
if max(max(abs(imag(a))))<1e-7
    a=real(a);
end

b=zeros(system_size,2);
b(1,1)=n2_i;
b(1,2)=n2_o;
b(4,1)=1;
b(6,2)=1;

aold=a;
bold=b;

% constructing matrices
system_size=8;            %system size is alwas 7
a=zeros(system_size,system_size);
a(1,1)=(-d2);
a(1,2)=-d1;
a(1,3)=-d0;
a(1,4)=n1_i+n2_i;
a(1,5)=n0_i;
a(1,6+1)=n1_o;
a(1,7+1)=n0_o;
a(2,1)=1;
a(3,2)=1;

a(4,4)=1;
a(5,4)=1;
a(5+1,4+1)=1;
a(7+1,6+1)=1;

aold=a;
% constructing matrices correction
system_size=8;            %system size is alwas 7
a=zeros(system_size,system_size);
a(1,1)=(-d2);
a(1,2)=-d1;
a(1,3)=-d0;
a(1,4)=+n2_i;
a(1,5)=n1_i;
a(1,6)=n0_i;
a(1,6+1)=n1_o;
a(1,7+1)=n0_o;
a(2,1)=1;
a(3,2)=1;

a(4,4)=1;
a(5,4)=1;
a(5+1,4+1)=1;
a(7+1,6+1)=1;

%to get only real numbers
if max(max(abs(imag(a))))<1e-7
    a=real(a);
end

b=zeros(system_size,2);
b(1,1)=n2_i;
b(1,2)=n2_o;
b(4,1)=1;
b(6,2)=1;

bold=b;
%Correction
b=zeros(system_size,2);
b(1,1)=0;
b(1,2)=n2_o;
b(4,1)=1;
b(6,2)=1;

%to get only real numbers
if max(max(abs(imag(b))))<1e-7
    b=real(b);
end

c=zeros(1,system_size);
c(1)=1;

% sys=ss(a,b,c,0,900);
% %[y,t,x,ysd] =impulse(sys);
% t=0:900:900*1000;
% u=zeros(length(t),2);
% u(1:1,1)=1;
% %u(1:end,1)=1;
% 
% [y,t,x]=lsim(sys,u,t);
% plot(t,y)
%% Simulation
la=30;          %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;
t= 0:sampling_time/3600:simulation_time/3600;                       %for adding time-scale to the plots
tt= 0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots

%Input discharges
u = zeros(2, length(simulation_time_vector)+la);
u(1,1) = 0;
u(2,14) = q_downstream-q;
u(2,15) = q_downstream-q;
u(2,16) = q_downstream-q;

x0 = zeros(system_size, 1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]=MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);

%% creating uncontrolled behaviour
x=x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;
for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

% figure
% plot(y6);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);
xres1=0;

for k=1:floor(simulation_time/sampling_time)

    %xtest=a*x+b*[u(1,1);D_time(1)];      %updating discharge in x
    xtest=a*x+b*[xres1(1);D_time(1)];      %updating discharge in x

    x=xtest;
    x(1,1)=y4(k)-Y0;                     %updating the water level difference every time step

    %Updating disturbance
    D_time(1:la) = u(2,k:k+la-1);
    
    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

   %  options =  optimoptions(@quadprog,'Display','off');
   % [xres,fval,exitflag]=quadprog(H,f,[],[],[],[],[],[],[],options );    
      xres1 = - inv(H)*f';
    xres2=xres1*0;
    xres2(1)=u(1,1)+xres1(1);
    for kk=2:length(xres1)
        xres2(kk)=xres2(kk-1)+xres1(kk);
    end
       xres= xres2;
 
   u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses
Error_max=max(abs(Error));                    %for analyses
Error_mean=mean(abs(Error));                  %for analyses

Switch = 0;
SettleTime=inf;
%Threshold = 0.003;
Threshold = 0.015;
for k=length(Error):-1:1
    if abs(Error(k))> Threshold && k==length(Error)
        SettleTime = inf;
    else    
    if abs(Error(k)) > Threshold && Switch ==0
        SettleTime=k*sampling_time;
        Switch = 1;
    end
    end
end

% fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
% fprintf('Relative steady state error = %f m\n', Error_steady_rel);
% fprintf('Relative maximum error      = %f m\n', Error_max_rel);
% fprintf('Relative mean error         = %f m\n', Error_mean_rel);
% 
% %%
% figure
% subplot(2,1,1)
% plot(t,y4,'--*');grid
% hold on
% plot(tt,y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(tt,u_m, '-*')
% hold on
% plot(tt,u_m2, '-*r');grid
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')