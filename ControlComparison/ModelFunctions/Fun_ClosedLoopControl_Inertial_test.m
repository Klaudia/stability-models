function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel, Error_max, SettleTime, Error_mean,a ,b, c]=Fun_ClosedLoopControl_Inertial_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,NodeNumGiven,factor_sim_time,eMAVE,duMAVE)
% clear
% n=0.02;                     %Manning factor [-]
% Bw=3;                       %Bottom width [m]
% 
% close all
% 
% L=7000;
% q=1;
% m=0;
% Sb=0.0001;
% Y0=1.2;
% h_real_initial=Y0;
% q_downstream=1.5;
% factor_sim_time=30;
% eMAVE=0.05;
% duMAVE=0.2;
% kh=0;
% UseTravelTime=0;
% Mr_db=0;
% Om_r=0;
% NodeNumGiven=30;

[ae tau T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);

%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;      %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %simulation time [s]

%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand

%Internal Inertial model
ModelType=2;
[x,ax,bx,D,Hinit, a, b]=LinearInertialImp(m,Bw,Y0,n,q,Sb,L,sampling_time,NodeNumGiven,ModelType);
       
NodeNum=floor(NodeNumGiven/2)*2-1;
system_size=NodeNum-2; 
system_size=length(a); 

c=zeros(1,system_size);
c(end)=1;

% sysc=d2c(sys);
% 
% 
% [numtf, dentf]=ss2tf(a, b, c, [0 0],1);
% sysci=ss(numtf,[dentf 0]);
% 
% 
%  sys=ss(a,b,c,0,900);
% %[y,t,x,ysd] =impulse(sys);
% t=0:900:900*1000;
% u=zeros(length(t),2);
% u(1:1,1)=1;
% %u(1:end,1)=1;
% 
% [y,t,x]=lsim(sys,u,t);
% plot(t,y)

%% Simulation
la=30;      %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;
t=0:sampling_time/3600:simulation_time/3600;                       %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots

%Discharge input
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]=MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);

%% creating uncontrolled behaviour
x=x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time;
end

% figure
% plot(ttt,y6_real);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control
x=x0;
xmodel=x0;
y4(1)=h_real_initial;
D_time=zeros (la,1);
xres1=0;
for k=1:floor(simulation_time/sampling_time)
    
    D_time(1:la)=u(2,k:k+la-1);
%    xmodel=a*x+b*[u(1,1);D_time(1)];
     x=a*x+b*[xres1(1);D_time(1)];    %updating discharge in x

 %   x=xmodel;
    x(end)=y4(k)-Y0;        %updating the water level difference every time step
  
      
    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

   %  options =  optimoptions(@quadprog,'Display','off');
  %  [xres,fval,exitflag]=quadprog(H,f,[],[],[],[],[],[],[],options );    u(1,1:la)=xres';
        xres1 = - inv(H)*f';
    xres2=xres1*0;
    xres2(1)=u(1,1)+xres1(1);
    for kk=2:length(xres1)
        xres2(kk)=xres2(kk-1)+xres1(kk);
    end
       xres= xres2;
          u(1,1:la)=xres';

    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses
Error_max=max(abs(Error));                    %for analyses
Error_mean=mean(abs(Error));                  %for analyses

Switch = 0;
SettleTime=inf;
Threshold = 0.003;
%Threshold = 0.015;
for k=length(Error):-1:1
    if abs(Error(k))> Threshold && k==length(Error)
        SettleTime = inf;
    else    
    if abs(Error(k)) > Threshold && Switch ==0
        SettleTime=k*sampling_time;
        Switch = 1;
    end
    end
end

% fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
% fprintf('Relative steady state error = %f m\n', Error_steady_rel);
% fprintf('Relative maximum error      = %f m\n', Error_max_rel);
% fprintf('Relative mean error         = %f m\n', Error_mean_rel);
% 
% %%
% % figure
% subplot(2,1,1)
% plot(t,y4,'--*');grid
% hold on
% plot(tt,y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(tt,u_m, '-*')
% hold on
% plot(tt,u_m2, '-*r');grid
% legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','northeast')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')