clear all; close all; 
addpath('../Functions')
addpath('ModelFunctions')
%% flow parameters
ToSave=1;

TypeMatrix=[1 3 4 5 1 3 4 5];
TrapezoidalMatrix=[false false false false true true true true];

 TypeMatrix=[1 ];
 TrapezoidalMatrix=[false false false false true true true true];


Mbig=[];
Mgrbig=[];
for typecounter = 1:length(TypeMatrix)
type=TypeMatrix(typecounter);                 %type of channel [1/3/4/5]
trapezoidal=TrapezoidalMatrix(typecounter);      %trapezoidal case? [true/false]
positive=true;         %positive disturbance? [true/false]

[Y0,q,q_downstream,Sb,m] = Type(type,trapezoidal); 

%% variables to tune
factor_sim_time=30;     %simulation length parameter
eMAVE=0.010;            %tuning factor; Maximum Allowed Value Estimate of the water level deviation
duMAVE=0.02;            %tuning factor; Maximum Allowed Value Estimate of the change in discharge



eMAVEMatrixR = ones(6,4);
eMAVEMatrixT = ones(6,4);


% eMAVEMatrixR  = ...
% [ 0.0020   0.0050   0.0080  0.0180 ;
%  0.0004    0.001   0.004 0.025 ;
%  0.001     0.002  0.003  0.003;
%  0.003     0.005 0.007 0.016 ;
%  0.001 0.004 0.007  0.012;
%  0.003  0.004  0.005   0.032];

% eMAVEMatrixT =[    0.0002       0.001 0.0090  0.0100;
%  0.0002  0.001 0.014  0.015 ;
%   0.0004  0.0002    0.0004   0.0006;
%  0.0008   0.002  0.008  0.011 ;
%  0.0005    0.002    0.009      0.010 ;
%  0.003 0.002  0.004  0.008];


 eMAVEMatrixT =[ ...
     0.0014        0.005 0.7   0.8;
 0.0006   0.004    0.012         0.008    ;
  0.0006   0.006  0.0008 0.0006  ;
  0.0006  0.014  0.9  1 ;
 0.001   0.007    0.8    0.9 ;
 0.004      0.019  0.016     0.045];

eMAVEMatrixR  = ...
[ 0.004  0.001 0.05   1.1; 
 0.002   0.011 0.001   0.007;
 0.0003  0.01  0.017  0.009;
 0.001  0.003 0.015 1.4 ;
 0.0006  0.005 0.014   1.1; 
 0.014   0.03   0.02    0.06 ];




if trapezoidal == true
   if type ==1 
       for n = 1 : 6
           eMAVERow (n)= eMAVEMatrixT (n,type);
       end 
   else
       for n = 1 : 6
          eMAVERow (n)= eMAVEMatrixT (n,type-1);
       end
   end
 else  
       if type ==1 
         for n = 1 : 6
           eMAVERow (n)= eMAVEMatrixR (n,type);
         end 
       else
         for n = 1 : 6
           eMAVERow (n)= eMAVEMatrixR (n,type-1);
         end
   end
end   
   


%% constants 

n=0.02;                     %Manning factor [-]
Bw=3;                       %Bottom width [m]

h_real_initial=Y0;          %Initial downstream water height [m] 
L=7000;                     %Channel length [m]
kh=0;                       %Nothing
Mr_db=0;                    %Nothing - for IR model
Om_r=0;                     %Nothing - for IR model
UseTravelTime=0;            %Nothing - for IR model
NodeNumGiven=30;            %for Inertial model

% to switch from positive to negative step-disturbance
if positive==false
q_t=q;
q=q_downstream;
q_downstream=q_t;
end




%% gathering results from different internal models
%ID model
[t_ID,tt_ID,y4_ID,y_ana_ID,u_m_ID,u_m2_ID,Error_steady_abs_ID,Error_steady_rel_ID,Error_max_rel_ID,Error_mean_rel_ID, Error_max_ID, SettleTime_ID, Error_mean_ID]=Fun_ClosedLoopControl_ID_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVERow(1),duMAVE);

%IDZ model
[t_IDZ,tt_IDZ,y4_IDZ,y_ana_IDZ,u_m_IDZ,u_m2_IDZ,Error_steady_abs_IDZ,Error_steady_rel_IDZ,Error_max_rel_IDZ,Error_mean_rel_IDZ, Error_max_IDZ, SettleTime_IDZ, Error_mean_IDZ]=Fun_ClosedLoopControl_IDZ_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVERow(2),duMAVE);

%Inertial model
[t_In,tt_In,y4_In,y_ana_In,u_m_In,u_m2_In,Error_steady_abs_In,Error_steady_rel_In,Error_max_rel_In,Error_mean_rel_In, Error_max_In, SettleTime_In, Error_mean_In]=Fun_ClosedLoopControl_Inertial_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,NodeNumGiven,factor_sim_time,eMAVERow(3),duMAVE);

%Hayami model
%if (type ~=5) 
    [t_H,tt_H,y4_H,y_ana_H,u_m_H,u_m2_H,Error_steady_abs_H,Error_steady_rel_H,Error_max_rel_H,Error_mean_rel_H, Error_max_H, SettleTime_H, Error_mean_H]=Fun_ClosedLoopControl_hayami_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVERow(6),duMAVE);
%end

%Muskingum model
[t_M,tt_M,y4_M,y_ana_M,u_m_M,u_m2_M,Error_steady_abs_M,Error_steady_rel_M,Error_max_rel_M,Error_mean_rel_M, Error_max_M, SettleTime_M, Error_mean_M]=Fun_ClosedLoopControl_Muskingum_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVERow(5),duMAVE);

%IR model
[t_IR,tt_IR,y4_IR,y_ana_IR,u_m_IR,u_m2_IR,Error_steady_abs_IR,Error_steady_rel_IR,Error_max_rel_IR,Error_mean_rel_IR, Error_max_IR, SettleTime_IR, Error_mean_IR]=Fun_ClosedLoopControl_IR_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,Mr_db,Om_r,UseTravelTime,factor_sim_time,eMAVERow(4),duMAVE);


%% visualizing resutls
f=figure;
% subplot(2,1,1)
plot(tt_ID,y_ana_ID,'r','Linewidth',2)
hold on
plot(t_ID,y4_ID,'k',t_IDZ,y4_IDZ,'--k',t_In,y4_In,':k',t_IR,y4_IR,'-.k','Linewidth',2);grid 
hold on
plot(t_M,y4_M,'Color',[0.7 0.7 0.7],'Linewidth',2)
%if (type ~=5) && (type~=4 && trapezoidal~=1)
    hold on
    plot(t_H,y4_H,'-.k','Color',[0.7 0.7 0.7],'Linewidth',2)
%end
title('Water level (downstream)')
xlabel('time (h)')
ylabel('water level (m)')
if positive==true
    legend('Setpoint','ID', 'IDZ','Inertial','IR','Muskingum','Hayami','location','southeast')
else
    legend('Setpoint','ID', 'IDZ','Inertial','IR','Muskingum','Hayami','location','northwest')
end


% subplot(2,1,2)
% plot(tt_ID,u_m_ID,'-*',tt_IDZ,u_m_IDZ,'-*')
% hold on
% plot(tt_ID,u_m2_ID,'-*r',tt_IDZ,u_m2_IDZ,'-*r');grid
% % legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')

if Error_steady_abs_ID<0.01
    test_ID=true;
else
    test_ID=false;
end

if Error_steady_abs_IDZ<0.01
    test_IDZ=true;
else
    test_IDZ=false;
end

if Error_steady_abs_In<0.01
    test_In=true;
else
    test_In=false;
end

%if (type ~=5) && (type~=4 && trapezoidal~=1)
    if Error_steady_abs_H<0.01
        test_H=true;
    else
        test_H=false;
    end
%end

if Error_steady_abs_M<0.01
    test_M=true;
else
    test_M=false;
end

if Error_steady_abs_H<0.01
    test_H=true;
else
    test_H=false;
end

if Error_steady_abs_IR<0.01
    test_IR=true;
else
    test_IR=false;
end

% if (type ~=5) && (type~=4 && trapezoidal~=1)
%     error_table = table([Error_steady_abs_ID;Error_steady_rel_ID;Error_mean_rel_ID;Error_max_rel_ID;test_ID],[Error_steady_abs_IDZ;Error_steady_rel_IDZ;Error_mean_rel_IDZ;Error_max_rel_IDZ;test_IDZ],[Error_steady_abs_In;Error_steady_rel_In;Error_mean_rel_In;Error_max_rel_In;test_In],[Error_steady_abs_H;Error_steady_rel_H;Error_mean_rel_H;Error_max_rel_H;test_H],[Error_steady_abs_M;Error_steady_rel_M;Error_mean_rel_M;Error_max_rel_M;test_M],[Error_steady_abs_IR;Error_steady_rel_IR;Error_mean_rel_IR;Error_max_rel_IR;test_IR],'VariableNames',{'ID','IDZ','Inertial','Hayami','Muskingum','IR'},'RowNames',{'Absolute steady state error [m]','Relative steady state error [m]','Relative mean error [m]','Relative max error [m]','Suitable [1=Yes, 0=No]'}) %
% else
%     error_table = table([Error_steady_abs_ID;Error_steady_rel_ID;Error_mean_rel_ID;Error_max_rel_ID;test_ID],[Error_steady_abs_IDZ;Error_steady_rel_IDZ;Error_mean_rel_IDZ;Error_max_rel_IDZ;test_IDZ],[Error_steady_abs_In;Error_steady_rel_In;Error_mean_rel_In;Error_max_rel_In;test_In],['-';'-';'-';'-';'0'],[Error_steady_abs_M;Error_steady_rel_M;Error_mean_rel_M;Error_max_rel_M;test_M],[Error_steady_abs_IR;Error_steady_rel_IR;Error_mean_rel_IR;Error_max_rel_IR;test_IR],'VariableNames',{'ID','IDZ','Inertial','Hayami','Muskingum','IR'},'RowNames',{'Absolute steady state error [m]','Relative steady state error [m]','Relative mean error [m]','Relative max error [m]','Suitable [1=Yes, 0=No]'}) %
% end
%
% M1=[Error_steady_abs_ID;Error_steady_rel_ID;Error_mean_ID;Error_max_ID;test_ID];
% M2=[Error_steady_abs_IDZ;Error_steady_rel_IDZ;Error_mean_IDZ;Error_max_IDZ;test_IDZ];
% M3=[Error_steady_abs_In;Error_steady_rel_In;Error_mean_In;Error_max_In;test_In];
% M4=[Error_steady_abs_H;Error_steady_rel_H;Error_mean_H;Error_max_H;test_H];
% M5=[Error_steady_abs_M;Error_steady_rel_M;Error_mean_M;Error_max_M;test_M];
% M6=[Error_steady_abs_IR;Error_steady_rel_IR;Error_mean_IR;Error_max_IR;test_IR];

M1=[Error_steady_abs_ID;Error_mean_ID;Error_max_ID];
M2=[Error_steady_abs_IDZ;Error_mean_IDZ;Error_max_IDZ];
M3=[Error_steady_abs_In;Error_mean_In;Error_max_In];
M4=[Error_steady_abs_H;Error_mean_H;Error_max_H];
M5=[Error_steady_abs_M;Error_mean_M;Error_max_M];
M6=[Error_steady_abs_IR;Error_mean_IR;Error_max_IR];


%'VariableNames',{'ID','IDZ','Inertial','Hayami','Muskingum','IR'},'RowNames',{'Absolute steady state error [m]','Relative steady state error [m]','Relative mean error [m]','Relative max error [m]','Suitable [1=Yes, 0=No]'}) %
M=[M1 M2 M3 M4 M5 M6];
if trapezoidal==true
filename=strcat('Type' ,num2str(type), 'Trap.txt');
figname=strcat('Type' ,num2str(type), 'Trap');

else
filename=strcat('Type' ,num2str(type), 'Rect.txt');
figname=strcat('Type' ,num2str(type), 'Rect');

end

for line=1:3
MyList=M(line,:);
%MyList=0:10:100;
boundaries=linspace(min(MyList),max(MyList),6);
for kkk=1:length(MyList)
    if MyList(kkk)<boundaries(2)
    grades(line,kkk)=1;
    elseif MyList(kkk)<boundaries(3)
    grades(line,kkk)=2;
    elseif MyList(kkk)<boundaries(4)
    grades(line,kkk)=3;
    elseif MyList(kkk)<boundaries(5)
    grades(line,kkk)=4;
    else
        grades(line,kkk)=5;
    end
end
end

Mgr=round(mean(grades));

if ToSave==1
   %dlmwrite( filename ,M*100, 'precision','%3.1f', 'delimiter','&')
   saveas(f,figname)
   print(f, '-dpng',figname)
   
%    fileID = fopen(filename,'w');
%    formatSpec='Ste(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\  \n MAE(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\ \n  ME(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\ \n';
%    fprintf(fileID, formatSpec, M*100)
%       formatSpec='Mark & %i & %i & %i & %i & %i & %i \\\\  \n ';
%    fprintf(fileID, formatSpec, Mgr)
%    fclose(fileID)
   
   
end
Mbig=[Mbig; M];
Mgrbig=[Mgrbig; Mgr];
end

%%
if ToSave==2
    filename='Overall.csv';
   dlmwrite( filename ,Mbig*100, 'precision','%3.1f', 'delimiter','&')


k=1;
for k=1:4
if k==1
    type=1;
else 
    type=k+1;
end
    
filename=strcat('Type' ,num2str(type), '.txt');
ToPrint=[Mbig(k*3-2:k*3,:) Mbig((k+4)*3-2:(k+4)*3,:)]*100;
fileID = fopen(filename,'w');
formatSpec='Ste(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f  & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\ \n  MAE(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f  & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\ \n  ME(cm) & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f  & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f & %3.1f \\\\ \n';
fprintf(fileID, formatSpec, ToPrint')
formatSpec='Mark & %i & %i & %i & %i & %i & %i & %i & %i & %i & %i & %i & %i \\\\  \n ';
fprintf(fileID, formatSpec, [Mgrbig(k,:) Mgrbig(k+4,:)] )
fclose(fileID)
end

end


