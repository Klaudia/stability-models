function [ Time,Hm2 , Hm,HinitH] = FiniteVoumeModel11bck(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum, UseInitialization, HinitGiven )
%Finite volume model
%Klaudia Horvth, Delft, 2016.12.21.
%Modified: 2017.01.15.
% clear all
% close all
% %input variables
% SimTime=13500; %in seconds
% s=0.0003;
% L=7000;
% Bw=3;
% z=0;
% n=0.01835;
% n=0.03;
% s=0.0015;
% dt=10;
% SimLength=floor(SimTime/dt)+1;
% qu=ones(1,SimLength+1)*1.5;
% qu(2:floor(3600/dt))=3;
% 
% %qu(100:200)=2;
% %qu(600:700)=1;
% 
% qd=ones(1,SimLength+1)*1.5;
% h=3;
% NodeNum=1000;
% HinitGiven=0;
% UseInitialization=0;

%UseInitialization - it is zero if we ininitalize
%                  - it is 1 when we use an initialization
%                  - it is 2 when we create one, it is an initialisation
%                  run
%                  - if it is 3, we load a file with initial values that we
%                  created when it was less than 3

DownstreamLevel=h;


% If it is an initialisation run, it sets the downstream b.c. to water
% level
DischargeUpstreamBoundary=1;
if UseInitialization==2
    DischargeDownstreamBoundary=0;
else
    DischargeDownstreamBoundary=1;
end
%NodeNum=165;
%SimTime=1; %in seconds


QNum=NodeNum-1;  %The number of discharge nodes is one less than the water level nodes

%Geometry
Sb=s;
Elevation=L*s;
d=-linspace(Elevation,0,NodeNum);

Q=ones(1,QNum)*qd(1);
dxf=ones(1,QNum)*L/QNum;  % The space discretization for the flow nodes 
dxh=[dxf(2)/2  dxf(2:end) dxf(2)/2];  %There is a half distance node in the begin and end

Theta1=0.5;
Theta=0.5;
Theta1=1;
Theta=1;
%ch=20;
g=9.81;
%n=1/ch/h^(1/6);
ch=1/n*h^(1/6);    %This should be checked FRICTION!!!!!
T=Bw+z*h*2;
     a=(Bw+T)*h/2;
     P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
     %R=a/P;
ch=1/n*(a/P)^(1/6); %MOD 04.05

Cf=g/ch^2;
%Cf=0.04;
%Cf=0;
TimeStep=1;

SimLength=floor(SimTime/dt)+1;
if UseInitialization<3

ProfFactor=1;
%n=1/ch/h^(1/6);

q=qd(1);

%% Initial profile
dx=dxf(2);
NodeNumProfile=(NodeNum*2-1-1)*ProfFactor+1;
dxProfile=dx/2/ProfFactor;
HinitT2=SteadyState(L, z,Bw,n,s,q,h,NodeNum,0 );
HinitT=SteadyState(L, z,Bw,n,s,q,h,NodeNum,1 );
%Obtaining the normal depth
for i=1:NodeNumProfile
     T=Bw+z*h*2;
     a=(Bw+T)*h/2;
     P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
     R=a/P;
     fr=((q^2*T)/(g*a^3))^0.5;
     %ch=1/n*R^(1/6);
     Sf=q^2*(1/ch)^2/(a^2*R^(1));
     dydx=(s-Sf)/(1-fr^2);
     hnew=h-dxProfile*dydx;
     hmat(i)=h;
     h=hnew;
end
HinitH=hmat(end:-ProfFactor*2:1);
HinitF=hmat(end-1:-ProfFactor*2:1);


if UseInitialization==1
    H=HinitGiven;
    HinitH=HinitGiven;
else
    H=HinitH;
end

%% Compute the H-s in between for initialization
for m=1:QNum
    if Q(m)>0
        Hf(m)=H(m);
    elseif Q(m)<0
        Hf(m)=H(m+1);
    end
    
%     if Q(m)>0
%         Ksif(m)=Hf(m)-d(m);
%     elseif Q(m)<0
%         Ksif(m)=Hf(m+1)-d(m+1);
%     elseif Q(m)==0
%         Ksif(m)=Hf(m)-min(d(m),d(m+1));
%     end
end

%Hf=HinitF;

for m=1:NodeNum
   Ksi(m)=H(m)-d(m); 
end
%%
else
load startparam   
HinitH=1;
end
for TimeStep=1:SimLength
    Hm2(TimeStep,:)=H;

    %% Second step: compute A, V, u and Sf
    %Computing At, Qt - at water level points
    for m=1:NodeNum
         At(m)=(Bw+z*H(m))*H(m); 
         Ak(m)=dxh(m)*(Bw+z*H(m)*2);
        % Ak(m)=dxh(m)*(0.5*(Bw+z*H(m)*2)+Bw);

          %Compute Q at nodes, check the first and the last one
          if m>1 && m<NodeNum
             Qt(m)=0.5*(Q(m-1)+Q(m));
          elseif m==1
             Qt(m)=0.5*(Q(m)+Q(m));
          elseif m==NodeNum
             Qt(m)=0.5*(Q(m-1)+Q(m-1));
          end  
             Ut(m)=Qt(m)/At(m);
    end

    %Computing Af, Uf and Vf - at flow points
        if TimeStep==1
            for m=1:QNum
              Af(m)=(Bw+z*Hf(m))*Hf(m); 
              Rf(m)=Af(m)/( Bw + 2*Hf(m)*(1 + z^2)^0.5);
              Uf(m)=Q(m)/Af(m);
              Vf(m)=Af(m)*dxf(m);
              Sf(m)=dxf(m)*Bw;
            end
        else
          for m=1:QNum
             Rf(m)=Af(m)/( Bw + 2*Hf(m)*(1 + z^2)^0.5); %Mod 04.04.
             Vf(m)=Af(m)*dxf(m);
             Sf(m)=dxf(m)*Bw;
             %Sf(m)=dxf(m)*(Bw+z*Hf(m)*2); %This might be for trapez
          end
        end
    %

    %% Compute B and D

    for m=1:QNum
       %ch=1/n*h^(1/6);
       Cf=g/ch^2*(Vf(m)/Sf(m))/Rf(m);  %mod 04.04.
  %     Cf=g/(1/n*Hf(m)^(1/6))^2*(Vf(m)/Sf(m))/Rf(m);  %mod 04.04.
       B(m)=-Theta1*Qt(m+1)/Vf(m)+Theta1*Qt(m)/Vf(m)+Sf(m)/Vf(m)*Cf*abs(Uf(m));
%        D(m)=Qt(m+1)/Vf(m)*Ut(m+1)-(1-Theta1)*Qt(m+1)/Vf(m)*Uf(m)+Qt(m)/Vf(m)*Uf(m)*(1-Theta1)-Qt(m)/Vf(m)*Ut(m);
%        D(m)=-Qt(m+1)/Vf(m)*Ut(m+1)-(1-Theta1)*Qt(m+1)/Vf(m)*Uf(m)+Qt(m)/Vf(m)*Uf(m)*(1-Theta1)+Qt(m)/Vf(m)*Ut(m);
       D(m)=-Qt(m+1)/Vf(m)*Ut(m+1)...
            +Qt(m)/Vf(m)*Ut(m)...
            -(1-Theta1)*Qt(m+1)/Vf(m)*Uf(m)+Qt(m)/Vf(m)*Uf(m)*(1-Theta1);

       F(m)=(g*Theta/dxf(m))/(1/dt+B(m));
       R(m)=(Uf(m)/dt-(g*(1-Theta))/dxf(m)*(Ksi(m+1)-Ksi(m))+D(m))/(1/dt+B(m));
    end
    %%
    for m=2:NodeNum-1
        LHS1(m)=Ak(m)/dt*Ksi(m)-Theta*Af(m)*R(m)    -(1-Theta)*Af(m)*Uf(m);
        LHS2(m)=                Theta*Af(m-1)*R(m-1)+(1-Theta)*Af(m-1)*Uf(m-1);

        RHS2KSimMinus1(m)=-Theta*Af(m-1)*F(m-1);
        DiagTerm(m)=Ak(m)/dt+Theta*Af(m-1)*F(m-1)+Theta*Af(m)*F(m);
        RHS1KSimPlus1(m)=-Theta*Af(m)*F(m);

        LHS1B(m)=Ak(m)/dt*Ksi(m)-(1-Theta)*qu(TimeStep);
        LHS2B(m)=(1-Theta)*qd(TimeStep);
        DiagTermU(m)=Ak(m)/dt+Theta*Af(m)*F(m);
        DiagTermD(m)=Ak(m)/dt+Theta*Af(m-1)*F(m-1);
    end

    %% Building the matrix

    A=zeros(NodeNum,NodeNum);
    Bd=ones(NodeNum,1);

    for m=2:NodeNum-1
       A(m,m)= DiagTerm(m);
       A(m,m+1)=RHS1KSimPlus1(m);
       A(m,m-1)=RHS2KSimMinus1(m);
       Bd(m)=LHS1(m)+LHS2(m);
    end
    A(1,1)=1;
    A(NodeNum,NodeNum)=1;
 
    %Boundary conditions
%     Bd(1)=Hup;
%     Bd(NodeNum)=Hdown;
    if DischargeDownstreamBoundary==1
        Bd(NodeNum)=Ksi(NodeNum);
        Bd(NodeNum-1)=LHS1B(NodeNum-1)+LHS2(NodeNum-1)-Theta*qd(TimeStep+1);
        A(NodeNum-1,NodeNum)=0;
        A(NodeNum-1,NodeNum-1)=DiagTermD(NodeNum-1);
    end
    if DischargeUpstreamBoundary==1
        Bd(1)=Ksi(1);
        Bd(2)=LHS1(2)+LHS2B(2)+Theta*qu(TimeStep+1);
        A(2,1)=0;
        A(2,2)=DiagTermU(2);
    end
    
    %The calculation
    KsiNew= A\Bd;
    %Test

    %Calculation of the boundary values     
    KsiNew(1)=KsiNew(2)-(KsiNew(3)-KsiNew(2))/1;  %Think about this
    if DischargeDownstreamBoundary==1
    KsiNew(end)=KsiNew(end-1)+(KsiNew(end-1)-KsiNew(end-2))/1;
    else
    KsiNew(end)=DownstreamLevel;
    end
    %New variables
    for m=1:NodeNum
        Hnew(m)=KsiNew(m)+d(m);
      %  Anew(m)=(Bw+z*Hnew(m))*Hnew(m); 
    end

    for m=1:QNum
       Ufnew(m)=R(m)-F(m)*(KsiNew(m+1)-KsiNew(m));
       Qfnew(m)=Ufnew(m)*Af(m); %or should be divided with the new cross section...
          if Qfnew(m)>0
              Hfnew(m)=KsiNew(m)+d(m);
          elseif Qfnew(m)<0
              Hfnew(m)=KsiNew(m+1)+d(m+1);
          elseif Qfnew(m)==0
              Hfnew(m)=max(KsiNew(m),KsiNew(m+1))+min(d(m),d(m+1));
          end
          Afnew(m)=(Bw+z*Hfnew(m))*Hfnew(m); 
          Qfnew(m)=Ufnew(m)*Afnew(m); %or should be divided with the new cross section...
          if m==QNum   %This is weird if the downstream bc is h
          Qfnew(m)=qd(TimeStep+1); 
          Ufnew(m)=Qfnew(m)/Afnew(QNum-1);% why do we divide with not the same area?
          end
          if m==2
          Qfnew(m-1)=qu(TimeStep+1); 
          Ufnew(m-1)=Qfnew(m-1)/Afnew(2); % why do we divide with not the same area?
          end
    end

    for m=1:NodeNum
      if m>1 && m<NodeNum
         Qtnew(m)=0.5*(Qfnew(m-1)+Qfnew(m));
      elseif m==1
         Qtnew(m)=0.5*(Qfnew(m)+Qfnew(m));
      elseif m==NodeNum
         Qtnew(m)=0.5*(Qfnew(m-1)+Qfnew(m-1));
      end  

    end

    Hm(TimeStep,:)=Hnew;
    Qm(TimeStep,:)=Qfnew;

%% Preparing for the next step
    Qt=Qtnew;
    Ksi=KsiNew;
    Hf=Hfnew;
    Q=Qfnew;
    Uf=Ufnew;
    H=Hnew;
  %  At=Anew;
    Af=Afnew; %check this

end


if UseInitialization<3
save startparam Qt Ksi Hf Q Uf H Af ch
end
 Time=((0:SimLength-1)*dt);
%  close all
%  figure
%  plot(Hm2(end,:))
%  figure
%  plot(Hm2(:,end))
%  figure
%  plot(Hm2(:,1))
%  
 
