function fr=froude(y,q,n,b0,m)
g=9.81;
a=b0*y+m*y^2;
t=b0+2*y*m;
c=(g*a/t)^0.5;
v=q/a;
fr=v/c;

%% Calulation froude number 
% v [velocity]
% q [discharge]
% a [area]
% c [Kinematic wave, speed is celerity]
% t [reference top width]
